/** @file functions.h
 *  @brief Holds the definitions for erica_func.cpp and logan_func.cpp
 */

#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <iomanip>
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <vector>
#include <queue>
#include <algorithm>
#include <functional>
#include <iomanip>
#include <unordered_map>

using namespace std;

//prototypes 

/*! 
 * @brief Node structure
 */
struct Node{
    vector<pair<Node*,string>> links;   /*!< {person, movie} */
    string person;			/*!< name of the person stored in this Node */
    int path_length = 0;	/*!< distance from this node to the "bacon" node */
    pair<Node*,string> prev_person = {nullptr, ""}; /*!< {person, movie} of the 
														previous linked person*/
};

//Logan
int check_args(int argc);
Node* read_n_build(ifstream& fin, unordered_map<string,Node*> &names_map, string bacon, int& size);
vector<string> parse_line(string line, string &movie);

//Erica
void print_shortest_path(Node* start_person, Node* end_person);
void print_histogram(vector<vector<string>> hist, int size);
void build_histogram(Node* person, vector<vector<string>> &hist);
void print_longest_paths(vector<vector<string>> hist);

