/** @file logan_func.cpp
 *  @brief Holds all funcitions for logan
 */
#include "functions.h"

/*************************************************************************//**
 * @author Logan Larson
 * 
 * @par Description: 
 * check_args() - checks if there are 2 or 3 arguments passed into the 
 * command line and returns 1 if there is neither.
 * 
 * @param[in]      argc - the number of arguments entered in the command line
 * 
 * @returns 0 program ran successful.
 * @returns 1 there were to many or too few arguments
 * 
 ****************************************************************************/
int check_args(int argc)
{
    //Check if the user entered acceptable arguments
    if( argc < 2 || argc > 3)
    {
        cout << "Usage: ./main filename <actor/actress name>" << endl;
        return 1;
    }
    return 0;
}

/*************************************************************************//**
 * @author Logan Larson
 * 
 * @par Description: 
 * read_n_build() - reads in from the file line by line. Then the line is
 * parsed into a movie and a list of people. Then for each person, if is does
 * not exit is names_map, a node is created and inserted into the map. Then 
 * for each person, each person is connect by pointers with the respective
 * movie as the edge.
 * 
 * @param[in,out]   fin  - input file stream
 * @param[out]      names_map - an unordered map that holds all nodes
 * @param[in]       bacon - the name of person entered into cmd line 
 * @param[out]      size -the total number of unique names read from file
 * 
 * @returns Node* - a pointer to the person entered into the cmd line
 * 
 ****************************************************************************/
Node* read_n_build(ifstream& fin, unordered_map<string,Node*> &names_map, string bacon, int& size)
{ 
    Node* bacon_ptr = nullptr;      //pointer to person entered in cmd line
    string line;                    //the current line read from file
    string movie;                   //the name of the current movie
    vector<string> people;          //a list of the current people read in
    bool foundB = false;            //used to check if bacon_ptr is found
    unordered_map<string,Node*>::iterator mit;      //map iterator
    unordered_map<string,Node*>::iterator mit2;     //map itorator 2

    //read in till end of file
    while(fin.good())
    { 
        getline(fin,line);

        //parse line into movie and list of people
        people = parse_line(line, movie);    

        //insert all people into map        
        for(auto p:people)
        {
            //find current person but insert
            //if person does not already have a node
            mit = names_map.find(p);
            if(mit == names_map.end())
            {   
                //make a new node and initialize
                Node* new_node = new(nothrow) Node;
                new_node->person = p;
                names_map.insert({p,new_node});
                size++;

                //check if person is the person from cmd line
                if(p == bacon && !foundB)
                {
                    bacon_ptr = new(nothrow) Node;
                    bacon_ptr = new_node;
                    foundB = true;
                }
            }
        }

        //connect links
        for(auto p:people)
        {
            //find person in map
            mit = names_map.find(p);
            for(auto p2:people)
            {   
                //connect link if person is not itself
                if(p != p2)
                {
                    //find linking person in map and connect link
                    mit2 = names_map.find(p2);
                    mit->second->links.push_back({mit2->second, movie});
                }
            }
        }

        people.clear();
    }

    return bacon_ptr;
}


/*************************************************************************//**
 * @author Logan Larson
 * 
 * @par Description: 
 * parse_line() - first parses the movie from line, then parses out names
 * until there are no names left to parse.
 * 
 * @param[in]   line - the current line from file
 * @param[out]  movie - the current movie
 * 
 * @returns people - a vector of names
 * 
 ****************************************************************************/
vector<string> parse_line(string line, string &movie)
{
    int found_at;                   //used for finding characters in parsing
    vector<string> people;

    //parse for movie 
    found_at = line.find_first_of("/\n");
    movie = line.substr(0,found_at);
    line.erase(0,found_at+1);

    //parse all names read in
    found_at = line.find_first_of("/\n");
    while (!line.empty() && found_at != string::npos)
    {
        people.push_back(line.substr(0,found_at));                
        line.erase(0,found_at+1);
        found_at = line.find_first_of("/\n");
    }

    return people;