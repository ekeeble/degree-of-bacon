/**	@file erica_func.cpp
 *	@brief This file contains functions written by Erica Keeble
 */

#include "functions.h"


/*************************************************************************//**
 * @author Erica Keeble
 * 
 * @par Description: 
 * Given the start_person (prompted to the user), and the end_person (default 
 * bacon, changeable in the command line), find and print the shortest path 
 * from start to end.
 * 
 * @param[in]      start_person - person that the user entered
 * @param[in]      end_person - person entered in command line, default Bacon, Kevin
 * 
 ****************************************************************************/
void print_shortest_path(Node* start_person, Node* end_person)
{	
	Node* person_to_print = start_person;
	int degree = 0;				//distance between start and end person

	//check to make sure both people exist
	if(start_person == nullptr)
		return;
	
	if(end_person == nullptr)
	{
		cout << "Person entered in command line is not found in data file." << endl;
		return;
	}

	do
	{
		//print the person's name
		cout << person_to_print->person << endl << endl;
		//print their prev_person's movie
		cout << person_to_print->prev_person.second << endl << endl;
		//set the person equal to their prev_person
		person_to_print = person_to_print->prev_person.first;
		degree++;
		//repeat until we've reached the person we are backtracking to
	}while(person_to_print->person != end_person->person);
	
	cout << person_to_print->person << endl << endl;

	cout << "Degree of separation is " << degree << endl << endl << endl;
	
}

/*************************************************************************//**
 * @author Erica Keeble
 * 
 * @par Description: 
 * Builds a chart that has the number of people at each distance from the 
 * person given in the command line.
 * 
 * @param[in]		person - person entered in command line, default Bacon, Kevin
 * @param[out]      hist - histogram, the chart
 *
 ****************************************************************************/
void build_histogram(Node* person, vector<vector<string>> &hist)
{
	queue<Node*> all_peeps;				//queue used for Breadth First Search
	Node *person_to_push, *popped_person;
	int i = 0;

	hist[0].push_back(person->person);

	//if person is nullptr, output errror message
	if(person == nullptr)
	{
		cout << "Bacon person was not found in the data file" << endl;
		return;
	}
 
	person->path_length = -1;
	
	/*push all links from the first person onto the queue, with their path
	  length equal to 1 */
	for(i=0; i<person->links.size(); i++)
	{
		person_to_push = person->links[i].first;
		if(person_to_push->path_length == 0)
		{
			person_to_push->prev_person = make_pair(person, person->links[i].second);
			person_to_push->path_length = 1;
			all_peeps.push(person_to_push);
		}
	}

	//Repeat until we have put everyone through the queue. 
	while(!all_peeps.empty())
	{
		//Pop off person from queue.
		popped_person = all_peeps.front();
		all_peeps.pop();

		//Add popped person to hist[person's path_length]. 
		hist[popped_person->path_length].push_back(popped_person->person);
			
	    /*add all of the popped person's links to the queue, but only if they 
		 haven't yet been added. */
		for(i=0; i<popped_person->links.size(); i++)
		{
			person_to_push = popped_person->links[i].first;
			if(person_to_push->path_length == 0)
			{
				person_to_push->prev_person = {popped_person, popped_person->links[i].second};
				person_to_push->path_length = popped_person->path_length + 1;
				all_peeps.push(person_to_push);
			}
		}
	}

	return;
}

/*************************************************************************//**
 * @author Erica Keeble
 * 
 * @par Description: 
 * Prints the histogram that was found in the build_histogram function 
 * 
 * @param[in]      hist - the histogram that was found in build_histogram
 * @param[in]      size - the total number of people entered from the data file
 * 
 ****************************************************************************/
void print_histogram(vector<vector<string>> hist, int size) 
{
	/*Add up all the people in the histogram. Subtract from all people in the map. 
	  That is how many INF people will be listed */
	int peeps_in_hist = 0, INF_peeps, i=0;
	double avg_path_length = 0;

	cout << "   #       Freq" << endl << "-------------------" << endl;

	//print the histogram
	while(hist[i].size() != 0)
	{
		cout << "  " << i << setw(11) << hist[i].size() << endl;
		peeps_in_hist += hist[i].size();
		i++;
	}
		
	//print INF 
	INF_peeps = size - peeps_in_hist;
	cout << "Inf" << setw(11) << INF_peeps << endl << endl;

	//Print average path length
	i = 0;
	while(hist[i].size() != 0)
	{
		avg_path_length += (double)(hist[i].size() / (double)size) * i;
		i++;
	}

	cout << "Average Path Length: " << avg_path_length << endl << endl;
}


/*************************************************************************//**
 * @author Erica Keeble
 * 
 * @par Description: 
 * Prints all the people at the fathest distance from the person entered in 
 * the command line
 *
 * @param[in]      hist - the histogram that was found in build_histogram
 * 
 ****************************************************************************/
void print_longest_paths(vector<vector<string>> hist)
{
	//walk backwards through hist to find the first place that it isn't 0
	for(int i=19; i>=0; i--)
	{
		if(hist[i].size() != 0)
		{
			//print the vector of names stored in freq[i]
			for(auto s:hist[i])
				cout << s << endl;
			i = -1;	
		}		
	}
	return;