/***************************************************************************//**
 * @file main.cpp
 * @mainpage CSC 315 - Fall 2018
 *
 * @brief Main function for project 2 - Graph Problems
 *
 * @section course_section Course Information
 *
 * @authors Logan Larson, Erica Keeble
 * 
 * @date Oct. 26, 2018
 * 
 * @par Professor: Dr. Qiao
 *
 * @par Course: CSC 315 - Section 1 - Fall 2018
 *
 * @section program_section Program Description
 *
 * @par Description
 * 
 * The program intakes a file contianing movies and main actors or 
 * actresses in them. The program intakes a file name and an optional
 * actor/actress name from the command line. If no name is provided, 
 * "Bacon, Kevin" is set by default. This person is the "bacon" for 
 * the program.
 *
 * First, main checks if the command line arguments are correct and opens the
 * input file. Then read_n_build function reads in from the file line by line. 
 * Each line is parsed into a movie and a list of people. Then for each person,
 * if they do not exist in names_map, a node is created and inserted into the
 * map. Each person from that line is connect by pointers with the
 * respective movie as the edge, forming a graph. Then a frequency analysis is 
 * done on the graph in build_histogram function.
 *
 * A menu with four options is repeatedly printed until option 4(quit)
 * is entered. The first option prints a shortest path from a user entered 
 * name to the name entered in the command line. The second option prints a 
 * histogram with the frequency analysis. The third prints the longest shortest
 * path. After the menu exits, the program deallocates the map and closes 
 * the file. 
 *
 * @section compile_section Compile Section
 *
 * @par Build
   @verbatim
% make
   @endverbatim
 * @par Run
 * @verbatim
% ./main input_file <Bacon name>
   @endverbatim
 *       
 *
 * @bug The histogram numbers are slightly different, which also changes average path length
 * @bug If there is not enough RAM available on the computer, the program will be killed
 *
 * @par Revision History
 * git commit history
 * [GitHub Pages](https://gitlab.mcs.sdsmt.edu/7427118/csc315_fall2018_project2.git).
 *
 *****************************************************************************/

#include "functions.h"
using namespace std;


/*************************************************************************//**
 * @author Logan Larson, Erica Keeble
 * 
 * @par Description: 
 * First, main checks if the command line arguments are correct and opens the
 * input file. Then read_n_build() is called, which reads in the file and
 * builds a graph connects person to person with a movie as an edge.
 * Then a frequency analysis is done on the graph in build histogram.
 * Then a menu with four options is repeatedly printed until option 4(quit)
 * is entered. The first option prints a shortest path from a user entered 
 * name to the name entered in the command line. The second option prints a 
 * histogram. The third prints the longest shortest path. After the menu 
 * exits, the program deallocates the map and closes the file. 
 * 
 * @param[in]   argc - the count of command line arguments
 * @param[in]   argv - holds command line arguments
 * 
 * @returns 0 program ran successful.
 * @returns 1 incorrect amount of command line arguements
 * @return  2 files did not open
 * 
 ****************************************************************************/
int main(int argc, char** argv)
{
    //variables
    ifstream fin;                           //input filestream
    unordered_map<string,Node*> names_map;  //unordered map of nodes
    Node* bacon_ptr = nullptr;              //pointer to person from cmd line
    string bacon = "Bacon, Kevin";          //name of person from cmd line
    int total_size = 0;                     //number of unique names in file
    char choice;                            //user entered menu choice
    vector<vector<string>> hist;            //Histogram

    hist.resize(20);            

    //check argumets
    if(check_args(argc) == 1)
        return 1;

    //open and check file
    fin.open(argv[1]);
    if(!fin)
    {
        cout << "Could not open file." << endl;
        return 2;
    }        

    //Check if a person was entered into cmd ("Bacon, Kevin" by default)
    if(argc == 3)
        bacon = argv[2];

    //Read in file and build graph
    bacon_ptr = read_n_build(fin, names_map, bacon, total_size);

    //Build histogram        
    build_histogram(bacon_ptr,hist);
    
    //Loops menu
    do
    {
        //Print menu
	cout << "1. Print degree of separation and shortest path" << endl;
	cout << "2. Print histogram" << endl;
	cout << "3. Print longest shortest paths" << endl;
	cout << "4. Quit" << endl;
        cout << "Enter choice [1-4]: " ;
        cin >> choice;

        if(choice == '1')
        {
            //Find shortest Path from person to cmd line person
            //Get person from user
            Node* input_person = nullptr;
            string name;
            cout << "Enter actor or actress: ";
            cin.ignore();
            getline(cin, name);

            //Check if person exits
            if( names_map.count(name) != 0)
                input_person = names_map.find(name)->second;
            else
                cout << "Person not found" << endl;

            print_shortest_path(input_person, bacon_ptr);
        }
        else if(choice == '2')
        {
            print_histogram(hist, total_size);
        }
        else if(choice == '3')
        {
            print_longest_paths(hist);
        }
        else if(choice == '4' )
        {
            //Quit program
            cout << "Quiting..." << endl;
        }
        else
        {
            //Invalid choice
            cout << "Invalid Choice" << endl;
        }
        cout << endl;
    }while(choice != '4');


    //Deallocate memory		
    for(auto n:names_map)
        delete n.second;

    fin.close();

    return 0;
}
